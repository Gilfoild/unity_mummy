﻿using UnityEngine;
using System.Collections;

public class Spider_move : MonoBehaviour
{

    CharacterController spider_controller;
    public Transform target; //мумия - цель которую будут преследовать
    public int speed_spider; //скорость перемещения паука
    public int rotation_speed; // скорость поворота
    public float maxDist; //максимальное приблежение к цели
    private float curDist; //текущаю дистанция
    public float reactDist; //дистанция, на которой монстр реагирует
    private Transform myTransform; // Временная переменная для хранения ссылки на свойство transform
    //Анимации
    public Animator anim;

 
    public enum MonsterStat
    {
        idle,
        walkPlayer,
    }

    private MonsterStat _monsterStat;

    void Awake()
    {
        myTransform = transform;
    }

    
    // Use this for initialization
    void Start()
    {
        GameObject go = GameObject.FindGameObjectWithTag("Player"); //  ищем по тэгу 
        target = go.transform; //ставим прицел      
        anim = GetComponent<Animator>();
       // anim[walk.name].speed = walk_speed;
       // anim[idle.name].speed = idle_speed;    
    }

    // Update is called once per frame
    void Update()
    {
        curDist = Vector3.Distance(target.position, myTransform.position);

        //если позволяет дистанция двигаемся к цели(проверка на минимальную дистанцию)

        if ((curDist >= maxDist) && (curDist <= reactDist))
        {
            _monsterStat = MonsterStat.walkPlayer;
        }

        else
        {
            _monsterStat = MonsterStat.idle;
        }
        
        switch (_monsterStat)
        {
            case MonsterStat.idle: //чертим линию
                //Debug.DrawLine(target.position, myTransform.position, Color.yellow);
                anim.SetBool("idle", true);
                anim.SetBool("walk", false);
                
                break;

            case MonsterStat.walkPlayer:
               // Debug.DrawLine(target.position, myTransform.position, Color.red);
                // Разворачиваемся в сторону игрока
                myTransform.rotation = Quaternion.Slerp(myTransform.rotation, Quaternion.LookRotation(target.position - myTransform.position), rotation_speed * Time.deltaTime);
                //двигаемся к цели
                myTransform.position += myTransform.forward * speed_spider * Time.deltaTime;

                anim.SetBool("walk", true);
                anim.SetBool("idle", false);

                break;

        }
  
    }
}

