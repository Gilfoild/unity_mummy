﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour {

    private GameObject count;
	// Use this for initialization
	void Start () {


        GetComponent<LoadScene>().enabled = false;
        this.gameObject.SetActive(false);
        
       


    }
	
	// Update is called once per frame
	void FixedUpdate () {

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        this.gameObject.SetActive(false);
        GetComponent<LoadScene>().enabled = false;
        
    }
    


}
