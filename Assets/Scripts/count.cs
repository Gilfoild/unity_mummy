﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class count : MonoBehaviour
{
    private GameObject textObject;
    public int counter = 0;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        textObject = GameObject.Find("CounterText");
    }

    void Awake()
    {
        //player = GameObject.Find("mummy_rig");
        //var count = GameObject.Find("mummy_rig").GetComponent<Waypoint_mummy>().count;

        var objCount = GameObject.FindGameObjectsWithTag("Counter").Length;
        if (objCount > 1)
        {
            Destroy(gameObject);
        }
        textObject = GameObject.Find("CounterText");
    }

    void Update()
    {
        textObject.GetComponent<Text>().text = "Bonus: " + counter.ToString();
    }
}
