﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Waypoint_mummy : MonoBehaviour
{
    Game.Network netConnection { get; set; }

    public GameObject Mummy;
    public GameObject Waypoint_1;
    public GameObject Waypoint_2;
    public GameObject Waypoint_3;
    public GameObject Waypoint_4;
    public GameObject Waypoint_5;
    public GameObject Waypoint_6;
    public GameObject Waypoint_7;
    public GameObject Waypoint_8;
    public GameObject Waypoint_9;
    public GameObject Waypoint_10;
    public GameObject Waypoint_11;
    public GameObject Waypoint_12;
    public GameObject Waypoint_13;
    public GameObject Waypoint_14;
    public GameObject Waypoint_15;
    public GameObject Waypoint_16;
    public GameObject Waypoint_17;
    public GameObject Waypoint_18;
    public GameObject Waypoint_19;
    public GameObject Waypoint_20;
    public GameObject Waypoint_21;
    public GameObject Waypoint_22;
    public GameObject Waypoint_23;
    public GameObject Waypoint_24;
    public GameObject Waypoint_25;
    public GameObject Waypoint_26;
    public GameObject Waypoint_27;
    public GameObject Waypoint_28;
    public GameObject Waypoint_29;
    public GameObject Waypoint_30;
    public GameObject Waypoint_31;
    public GameObject[] waypoints;
    public GameObject Timer;
    public Animator animator;
    private GameObject SceneLoad;
    private GameObject TimeTimer;
    private GameObject CountText;

    public int num = 0;
    public State state;
    public float time;
    public float max_speed = 5f; //максимальная скорость
    public float rotation_speed;
    public bool _switchWaypoint;
    public bool go = true;
    public bool Rotating = true;
    private float move_speed; //текущая скорость
    public Text speed_text;
    //public bool isMoving = true;

    float endTime;
    // поворот персонажа
    public float targetAngle = 90.0f; //угол поворота к кот стремимся
    private float startAngle; // изначальный угол 

    private float stepXRotation;
    private float stepZ;
    private float stepYRotation;
    private float stepY;
    private float finalXRotation = 11;
    private float finalZ = 2.5f;
    private float finalYRotation = 180;
    private float finalY = 1;
    Transform cameraTransform;
    private const float timeToEnd = 1.5f;

    public enum State
    {
        Idle,
        Way1,
        Way2,
        Way3,
    }



    // Use this for initialization
    void Start()
    {

        netConnection = Game.Network.Instance;

        waypoints = new[] { Waypoint_1, Waypoint_2, Waypoint_3, Waypoint_4, Waypoint_5, Waypoint_6, Waypoint_7, Waypoint_8, Waypoint_9, Waypoint_10, Waypoint_11,
        Waypoint_12, Waypoint_13, Waypoint_14, Waypoint_15, Waypoint_16, Waypoint_17, Waypoint_18, Waypoint_19, Waypoint_20, Waypoint_21, Waypoint_22, Waypoint_23,
        Waypoint_24, Waypoint_25, Waypoint_26, Waypoint_27, Waypoint_28, Waypoint_29, Waypoint_30, Waypoint_31 };
        animator = GetComponent<Animator>();
        //запоминаем начальный угол

        SceneLoad = GameObject.Find("SceneLoad");
        TimeTimer = GameObject.Find("Timer");


        cameraTransform = transform.GetChild(0).GetComponent<Transform>();
        
        stepXRotation = (finalXRotation - cameraTransform.localRotation.x)/timeToEnd;
        stepZ = (finalZ - cameraTransform.localPosition.z)/timeToEnd;
        stepYRotation = (finalYRotation - cameraTransform.localRotation.y)/timeToEnd;
        stepY = (finalY-cameraTransform.localPosition.y)/timeToEnd;
    }

    private void Awake()
    {
        CountText = GameObject.Find("CounterCanvas");
    }

    private float lasFrameTime;
    // Update is called once per frame
    void Update()
    {

        CountText.SetActive(false);

        if (isGameEnd)
        {

            animator.SetBool("isWalk", false);
            animator.SetBool("isRun", false);
            animator.SetBool("isIdle", true);
            TimeTimer.GetComponent<timer>().enabled = false;
            TimeTimer.SetActive(false);

            var timeLeft = Time.time - endTime;
            var stepTime = timeToEnd/(Time.time - lasFrameTime);

            var x = cameraTransform.localPosition.x;
            var y = cameraTransform.localPosition.y;
            var z = cameraTransform.localPosition.z;

            var xRot = cameraTransform.localRotation.x;
            var yRot = cameraTransform.localRotation.y;
            var zRot = cameraTransform.localRotation.z;

            cameraTransform.localPosition = new Vector3(x, y+stepY * stepTime, z+stepZ * stepTime);
            cameraTransform.localRotation = new Quaternion(xRot+stepXRotation * stepTime, yRot+stepYRotation * stepTime, zRot, cameraTransform.rotation.w);

            if (timeLeft > timeToEnd)
            {
                SceneLoad.SetActive(true);
                SceneLoad.GetComponent<LoadScene>().enabled = true;
            }
        }
        lasFrameTime = Time.time;

        if (Timer.activeInHierarchy == false)
        {
            CountText.SetActive(true);
            //CountText.GetComponent<Text>().text = "Bonus: ";

            if (netConnection.close)
            {
                Application.Quit();
            }

            float border = netConnection.ReadBorder(); //нижняя граница
            float top = netConnection.ReadTop();//верхняя граница
            float value = netConnection.ReadValue();//высота столбика

            if (value == 0 && border == 0 && top == 0)
            {
                return;
            }
            //рассчет скорости
            //значение ниже нижней границы / выше верхней
            if (value < border)
            {
                if (border - value <= 15)// , желтая граница  на 10 ниже border и на 10 выше top
                {
                    move_speed = max_speed * (1 - (border - value) / 100); //должна быть больше
                    animator.SetBool("isRun", false);
                    animator.SetBool("isWalk", true);
                    // animator.SetBool("isDeath", false);
                }
                else
                {
                    move_speed = max_speed * (1 - (border - value) / 100) * (1 - (border - value) / 100);
                    animator.SetBool("isRun", false);
                    animator.SetBool("isWalk", true);
                    // animator.SetBool("isDeath", true);
                }
            }
            else if (value > top)
            {
                if (value - top <= 15)// , желтая граница  на 10 ниже border и на 10 выше top
                {

                    move_speed = max_speed * (1 - (value - top) / 100); //должна быть больше 

                    animator.SetBool("isRun", false);
                    animator.SetBool("isWalk", true);
                    // animator.SetBool("isDeath", false);
                }
                else
                {
                    move_speed = max_speed * (1 - (value - top) / 100) * (1 - (value - top) / 100);

                    animator.SetBool("isRun", false);
                    animator.SetBool("isWalk", true);
                    //animator.SetBool("isDeath", true);
                }
            }
            //когда значение попадает в границы столбика
            else
            {
                move_speed = max_speed; // зеленая
                animator.SetBool("isWalk", false);
                // animator.SetBool("isDeath", false);
                animator.SetBool("isRun", true);
            }
        }
       

        float dist = Vector3.Distance(gameObject.transform.position, waypoints[num].transform.position);

        if (go)
        {
            if (dist > 2f)
            {
                Move();

            }
            else
            {

                if (num + 1 == waypoints.Length)
                {
                    return;
                }
                else
                {
                    num++;
                }

            }
        }
    }

    public void Move()
    {
        float distance = Vector3.Distance(Mummy.transform.position, waypoints[num].transform.position);
        Mummy.transform.rotation = Quaternion.Lerp(Mummy.transform.rotation, Quaternion.LookRotation(waypoints[num].transform.position - Mummy.transform.position), move_speed * Time.deltaTime);
        Debug.DrawLine(waypoints[num].transform.position, Mummy.transform.position, Color.green);
        Mummy.transform.position += Mummy.transform.forward * move_speed * Time.deltaTime;
        if (distance < 1f) _switchWaypoint = true;

    }
   
    private bool isGameEnd = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick up"))
        {
            CountText.GetComponent<count>().counter = CountText.GetComponent<count>().counter + 1;
            GameObject.Find("CounterText").GetComponent<Text>().text = String.Format("Bonus: {0}", CountText.GetComponent<count>().counter);
            other.gameObject.SetActive(false);
            isGameEnd = true;
            endTime = Time.time;            
            
        }
        
    }
    

}
    


        
       
      
       
