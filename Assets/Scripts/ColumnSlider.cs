﻿using UnityEngine;
using UnityEngine.UI;
using System.Net.Sockets;
using System.Collections;

public class ColumnSlider : MonoBehaviour {
      
    public int startingFill = 0; //сатртовое значение столбика
    public int curentFill; //текущее значение столбика
    
    public Slider BosSlider; //ссылка на слайдер
    public Slider TopSlider; //ссылка на слайдер
    public Slider BottomSlider; //ссылка на слайдер
    public RectTransform newFill; //сслыка на новый rectransform (дочерний FillArea)
                              
    

	// Use this for initialization
	void Start () {

        curentFill = startingFill;
        BosSlider.fillRect = newFill;
      
    }
	
	// Update is called once per frame
	void Update () {

        var barValue = Game.Network.Instance.ReadValue(); //высота столбика
        var barBorder = Game.Network.Instance.ReadBorder(); //нижняя  граница
        var barTop = Game.Network.Instance.ReadTop(); //верхняя граница

	    TopSlider.value = barTop;
	    BottomSlider.value = barBorder;

        if (barValue == 0 && barBorder == 0)
        {
            return;
        }

        if (barValue < barBorder)
        {
            if (barBorder - barValue <= 15)
            {
                curentFill = barValue;
                BosSlider.value = curentFill;
                newFill.GetComponent<Image>().color = Color.yellow; //изменяем цвет

            }
            else
            {
                curentFill = barValue;
                BosSlider.value = curentFill;
                newFill.GetComponent<Image>().color = Color.red;
            }
        }
        else if (barValue > barTop)
        {
            if (barValue - barTop <= 15)
            {
                curentFill = barValue;
                BosSlider.value = curentFill;
                newFill.GetComponent<Image>().color = Color.yellow;
            }
            else
            {
                curentFill = barValue;
                BosSlider.value = curentFill;
                newFill.GetComponent<Image>().color = Color.red;
            }
        }

        else
        {
            curentFill = barValue;
            BosSlider.value = curentFill;
            newFill.GetComponent<Image>().color = new Color(0,.5f,0);
        }

            
          
        }
    }

