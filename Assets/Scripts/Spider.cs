﻿using UnityEngine;
using System.Collections;

public class Spider : MonoBehaviour {

    public GameObject _spider;
    public Animator _anim;
    public GameObject _mummy;

    private float _speed = 6f;
    private float _jump = 6f;
    private float _rotation = 6f;

    public float _maxDist; //макс дистанция
    private float _curDist; // текущая дистанция
    private float __reactDist = 1f; // дистанция, на которой прыгает на игрока
        
	// Use this for initialization
	void Start () {

        _anim = GetComponent<Animator>();
        	
	}
	
	// Update is called once per frame
	void Update () {

        _curDist = Vector3.Distance(_mummy.transform.position, _spider.transform.position);

        if(_curDist != __reactDist )
        {
            _spider.GetComponent<Animator>().SetBool("idle", true);
        }

        else
        {
            _spider.transform.rotation = Quaternion.Slerp(_spider.transform.rotation, Quaternion.LookRotation(_mummy.transform.position - _spider.transform.position), _rotation * Time.deltaTime );
            _spider.GetComponent<Animator>().SetBool("jump", true);
        }
    
	
	}
}
