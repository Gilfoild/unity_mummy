﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using System.Collections;
using System;

public class timer : MonoBehaviour
{
    
    private Text timerText;
    private Text GameOverText;
    private float startTime = 0;
    private float levelTime = 60;
    private GameObject playerMummy;
    public GameObject SceneLoad;
   
    
    public Animator an;


    // Use this for initialization
    void Start()
    {
        var goTextObject = GameObject.Find("GameOverText ");
        GameOverText = goTextObject.GetComponent<Text>();
        an = GameObject.Find("mummy_rig").GetComponent<Animator>();
        timerText = GameObject.Find("TimerText").GetComponent<Text>();
        playerMummy = GameObject.Find("mummy_rig");
        SceneLoad = GameObject.Find("SceneLoad");
        startTime = Time.time+5;
        
    }

    // Update is called once per frame
    void Update()
    {
        
        float t = levelTime - (Time.time - startTime);

        string minutes = ((int)t / 60).ToString();
        string seconds = (t % 60).ToString(t <= 10 ? "f1" : "f0");

        timerText.text = minutes + ":" + seconds;

        if (t <= 10 && t > 0)
        {
            var secPart = t - Math.Truncate(t);
            var fSize = Convert.ToInt32(50 - 20 * secPart * secPart);
            timerText.GetComponent<Text>().fontSize = fSize;
            timerText.GetComponent<Text>().color = Color.red;

        }
        else if (t <= 0)
        {
            //GameOverText.text = "Game Over";
            //GameOverText.color = Color.gray;
            //GameOverText.fontSize = 100;

            timerText.text = "00 : 00";
            
            an.SetBool("isWalk", false);
            an.SetBool("isRun", false);
            an.SetBool("isIdle", true);


            playerMummy.GetComponent<Waypoint_mummy>().enabled = false;
            //mummy_rig = waypoitScript.GetComponent<Waypoint_mummy>();
            // waypointScript.SetActive( false);

            //SceneLoad.GetComponent<LoadScene>().enabled = true;
            //SceneLoad.SetActive(true);

            SceneLoad.SetActive(true);
            SceneLoad.GetComponent<LoadScene>().enabled = true; 
            this.gameObject.SetActive(false);
           

        }
        
    }

}

