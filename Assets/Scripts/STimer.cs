﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class STimer : MonoBehaviour
{

    public Text readyText;
    private Text goText;
    public GameObject Timer;
    public Canvas ReadyCanvas;
    public Canvas timeCanvas;
    private float startReady;
    private float levelReady = 5;
        
    // Use this for initialization
    void Start()
    {
       
        timeCanvas.GetComponent<Canvas>().enabled = false;
        startReady = Time.time;
        var readyTextObject = GameObject.Find("goText");
        goText = readyTextObject.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        
        float go = levelReady - (Time.time - startReady);
        string go_sec = Math.Ceiling(go - 1).ToString();

        if (go <= 4 && go > 1)
        {
            readyText.text = go_sec;
            var _secPart = go - Math.Truncate(go);
            var _fSize = Convert.ToInt32(80 - 20 * _secPart * _secPart);
            readyText.GetComponent<Text>().fontSize = _fSize;
        }
        else if (go <= 1)
        {
            readyText.text = go_sec;
            goText.text = "GO";
            goText.color = Color.green;
            var _secPart = go - Math.Truncate(go);
            var _fSize = Convert.ToInt32(80 - 10 * _secPart * _secPart);
            goText.fontSize = _fSize;

            readyText.GetComponent<Text>().enabled = false;

        if( go <= 0)
            {                
                this.gameObject.SetActive(false);
                Timer.SetActive(true);
                ReadyCanvas.GetComponent<Canvas>().enabled = false;
                timeCanvas.GetComponent<Canvas>().enabled = true;
                //Timer.GetComponent<timer>().startTime = Time.time;

            }
           
           
        }
    }
}



