﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game
{
    class Network
    {
        TcpListener server;
        //public int[] data { get; private set; }
        static object locker = new object();
        int value = 0;   // Значение в процентах
        int border = 0;  //Нижняя Граница зеленой зоны
        int top = 0;    //Верхняя Граница зеленой зоны
        int progress = 0; //Инкрементируемое значение прогресса

        private static System.Timers.Timer aTimer;

        Thread netThread;
        const int LENGTH_BYTE_POS = 8;
        const int DATA_BYTE_POS = 14;
        public Network()
        {

            close = false;
            InProc = true;
            netThread = new Thread(new ThreadStart(NetThread));
            netThread.IsBackground = true;
            netThread.Start();

            aTimer = new System.Timers.Timer();
            aTimer.Interval = 50;
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }
        bool InProc;
        public void Stop()
        {
            server.Stop();

            InProc = false;
            netThread.Abort();
        }
        //double old_res;
        public bool close { get; set; }

        void NetThread()
        {
            try
            {
                // Set the TcpListener on port 13000.
                Int32 port = 60000;
                IPAddress localAddr = IPAddress.Any;

                // TcpListener server = new TcpListener(port);
                server = new TcpListener(localAddr, port);

                // Start listening for client requests.
                server.Start();

                // Buffer for reading data
                Byte[] bytes = new Byte[26];
                // data = new int[4];

                // Enter the listening loop.
                while (InProc)
                {
                    Console.Write("Waiting for a connection... ");

                    // Perform a blocking call to accept requests.
                    // You could also user server.AcceptSocket() here.
                    TcpClient client = server.AcceptTcpClient();
                    Console.WriteLine("Connected!");

                    //data = new int[4] { 0, 0, 0, 0 };
                    SetValue(0);
                    SetBorder(0);

                    // Get a stream object for reading and writing
                    NetworkStream stream = client.GetStream();

                    int i;

                    // Loop to receive all the data sent by the client.
                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        if (((int)bytes[LENGTH_BYTE_POS + 1] == 0) && ((int)bytes[LENGTH_BYTE_POS + 2] == 0)
                            && ((int)bytes[LENGTH_BYTE_POS + 3] == 0) && ((int)bytes[LENGTH_BYTE_POS + 4] == 0)
                            && ((int)bytes[LENGTH_BYTE_POS + 5] == 0) && ((int)bytes[LENGTH_BYTE_POS + 6] == 0)
                            && ((int)bytes[LENGTH_BYTE_POS + 7] == 0) && ((int)bytes[LENGTH_BYTE_POS + 8] == 0))
                            close = true;

                        if (
                            (int)bytes[0] == 14 &&  //Заголовок 4 байта
                            (int)bytes[1] == 15 &&
                            (int)bytes[2] == 176 &&
                            (int)bytes[3] == 177 &&

                            (int)bytes[4] == 219 && //Маркер начала 1 байт
                            (int)bytes[12] == 0 &&    // 0 – номер канала

                            (int)bytes[9] != 255 &&  //Проверка первых 4 байт пакета на значение FF
                            (int)bytes[10] != 255 &&
                            (int)bytes[11] != 255 &&
                            (int)bytes[12] != 255)
                        {
                            SetValue((int)bytes[13]);
                            SetBorder((int)bytes[15]);
                            SetTop((int)bytes[16]);
                            //if ((int)bytes[13] >= (int)bytes[15] && (int)bytes[15]!=0)
                            //{
                            //    ProgressUp();
                            //}
                        }


                    }

                    // Shutdown and end connection
                    client.Close();
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                // Stop listening for new clients.
                server.Stop();
            }
        }

        private void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            int b = ReadBorder();
            int v = ReadValue();
            int t = ReadTop();
            if (v >= b && b != 0 && v <= t)
            {
                ProgressUp();
            }
        }


        public void ProgressUp()
        {
            lock (locker)
            {
                if (progress + 2 <= Int32.MaxValue)
                    progress++;
            }
        }

        public void ProgressNull()
        {
            lock (locker)
            {
                progress = 0;
            }
        }
        public int ReadProgress()
        {
            lock (locker)
            {
                return progress;
            }
        }
        public int ReadValue()
        {
            lock (locker)
            {
                return value;
            }
        }
        public int ReadBorder()
        {
            lock (locker)
            {
                return border;
            }
        }
        public int ReadTop()
        {
            lock (locker)
            {
                return top;
            }
        }
        public void SetValue(int v)
        {
            lock (locker)
            {
                value = v;
            }
        }
        public void SetBorder(int b)
        {
            lock (locker)
            {
                border = b;
            }
        }

        public void SetTop(int t)
        {
            lock (locker)
            {
                top = t;
            }
        }


    }
}


//Заголовок 4 байта
//(0) 14
//(1) 15
//(2) 176
//(3) 177
//Маркер начала 1 байт
//(4) 219
//Размер 4 байта
//(5) 0
//(6) 0
//(7) 0
//(8) 8
//Тело пакета
//(9) 4 – номер группы
//(10) 0 
//(11) 1 – количество активных каналов
//(12) 0 – номер канала
//(13) 100 – Значение
//(14) 44 – Отклонение
//(15) 38 – Нижняя граница
//(16) 56 – Верхняя граница

//Заголовок 4 байта
//(17) 14
//(18) 15
//(19) 176
//(20) 177
//Маркер конца пакета 1 байт
//(21) 220
//Размер 4 байта
//0
//0
//0
//8
